var http = require('http');
var url = require('url');
var fs = require('fs');

http.createServer(function (req, res) {
  var q = url.parse(req.url, true);
  console.log(q.pathname)
  if (q.pathname == "/sensors") {
    console.log("request recieved");
    res.writeHead(200, { "Content-Type": "text/plain" });
    getSensors(res)
    console.log("string sent");
  } else if (q.pathname == "/index.html") {
    var filename = "." + q.pathname;
    fs.readFile(filename, function (err, data) {
      if (err) {
        res.writeHead(404, { 'Content-Type': 'text/html' });
        return res.end("404 Not Found");
      }
      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.write(data);
      return res.end();
    });
  }else if(q.pathname=="/favicon.ico"){
}else{
    console.log("request recieved");
    res.writeHead(200, { "Content-Type": "text/plain" });
    getValues(q.pathname.substring(1), res)
    console.log("string sent");
  }
}).listen(8080);



const sqlite3 = require('sqlite3').verbose();


let db = new sqlite3.Database('./fuho.db', (err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log('Connected to the in-memory SQlite database.');
});



function getValues(sensor, res) {

  var values = {}
  avg(1)
  function avg(month) {

    sql = "SELECT AVG(value1) temperature FROM " + sensor + " WHERE datecolumn>? AND datecolumn <?";

    db.all(sql, [get_UTS(get_date_months_ago(month)), get_UTS(get_date_months_ago(month - 1))], (err, rows) => {
      if (err) {
        throw err;
      }
      rows.forEach((row) => {
        values[get_date_months_ago(month).getMonth()] = row.temperature / 100;
      });
      if (month == 12) {
        max();
      } else {
        avg(month + 1);
      }
    });
  }

  function max() {
    let sql = "SELECT datecolumn date, MAX(value1) temperature FROM " + sensor + " WHERE datecolumn>?";

    db.all(sql, [get_UTS_one_year_ago()], (err, rows) => {
      if (err) {
        throw err;
      }
      rows.forEach((row) => {
        values["maxDate"] = row.date;
        values["maxTemp"] = row.temperature / 100;
      });
      min();
    });
  }
  function min() {

    let sql = "SELECT datecolumn date, MIN(value1) temperature FROM " + sensor + " WHERE datecolumn>?";

    db.all(sql, [get_UTS_one_year_ago()], (err, rows) => {
      if (err) {
        throw err;
      }
      rows.forEach((row) => {
        values["minDate"] = row.date;
        values["minTemp"] = row.temperature / 100;
      });
      res.end(JSON.stringify(values))
    });
  }
}

function getSensors(res) {
  let sql = "SELECT id sensor, description description FROM id_descriptions";
  sensors={}

  db.all(sql, [], (err, rows) => {
    if (err) {
      throw err;
    }
    rows.forEach((row) => {
     sensors[row.sensor]=row.description;
    });
    res.end(JSON.stringify(sensors))
  });
}


function get_UTS_one_year_ago() {
  var date = new Date();

  date.setDate(date.getDate() - 365);
  return Math.round(date.getTime() / 1000);
}

function get_UTS(date) {
  return Math.round(date.getTime() / 1000);
}

function get_date_months_ago(months) {
  var date = new Date();
  var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);

  firstDay.setMonth(firstDay.getMonth() - months);
  return firstDay;
}

