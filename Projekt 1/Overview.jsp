<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Overview</title>
</head>
<body>
	<h1>Übersicht</h1>
	<h2>
		Mitglieder:
	</h2>
	<form action="Mitglieder.jsp" method="get">
		<button type="submit">anzeigen</button>
	</form>
	<h2>
		Kurse:
	</h2>
	<form action="Kurse.jsp" method="get">
		<button type="submit">anzeigen</button>
	</form>
	<h2>
		Unterkünfte:
	</h2>
	<form action="Unterkünfte.jsp" method="get">
		<button type="submit">anzeigen</button>
	</form>
	<h2>
		Verbindungen zwischen Mitgliedern und Kursen:
	</h2>
	<form action="MitgKurs.jsp" method="get">
		<button type="submit">anzeigen</button>
	</form>
</body>
</html>