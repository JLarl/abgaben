<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h3>
		Hier werden alle Mitglieder der eingegebenen Unterkunft angezeigt.
	</h3>
	<%
		String unterkunftsname;
		unterkunftsname = request.getParameter("unterkunftsname1");
	%>
	<%
		out.println("<h3>" + unterkunftsname + ":</h3>");
	%>
	<table>
		<thead>
			<tr>
				<th>MitgliedsID</th>
				<th>Vorname</th>
				<th>Nachname</th>
			</tr>
		</thead>
		<tbody>
			<%
				try {
					Class.forName("org.sqlite.JDBC");
					Connection c = DriverManager
							.getConnection("jdbc:sqlite:C:\\sqlite\\sqlite-tools-win32-x86-3200100\\alpenverein.db");
					Statement stmt = c.createStatement();
					ResultSet rs = stmt.executeQuery(
							"SELECT a.MitgliedsID, a.Vorname, a.Nachname FROM Mitglieder a INNER JOIN Unterkunfte b ON "
									+ "a.UnterkunftsID=b.UnterkunftsID WHERE b.Unterkunftsname='" + unterkunftsname + "'");

					while (rs.next()) {
						out.println("<tr>");
						out.println("<td>" + rs.getString("MitgliedsID") + "</td>");
						out.println("<td>" + rs.getString("Vorname") + "</td>");
						out.println("<td>" + rs.getString("Nachname") + "</td>");
						out.println("</tr>");
					}
					rs.close();
					c.close();
				} catch (SQLException e) {
				}
			%>
		</tbody>
	</table>
	<form action="Unterkünfte.jsp" method="get">
		<button type="submit">Zurück zur Unterkunftsübersicht</button>
	</form>
</body>
</html>