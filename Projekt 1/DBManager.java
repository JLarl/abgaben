import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class DBManager {
	private Connection c;
	Scanner sc = new Scanner(System.in);

	public DBManager() {
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:\\sqlite\\sqlite-tools-win32-x86-3200100\\alpenverein.db");
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void close() {
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void createTUnterkunft() {
		PreparedStatement stmt;
		String sql = "CREATE TABLE IF NOT EXISTS Unterkunfte(" + "UnterkunftsID INTEGER,"
				+ "Unterkunftsname TEXT NOT NULL," + "Baujahr INT NOT NULL," + "PRIMARY KEY(UnterkunftsID));";
		try {
			stmt = c.prepareStatement(sql);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void createTMitglied() {
		PreparedStatement stmt;
		String sql = "CREATE TABLE IF NOT EXISTS Mitglieder(" + "MitgliedsID INTEGER NOT NULL,"
				+ "Vorname TEXT NOT NULL," + "Nachname TEXT NOT NULL," + "UnterkunftsID TEXT NOT NULL,"
				+ "PRIMARY KEY(MitgliedsID)" + "FOREIGN KEY(UnterkunftsID) REFERENCES Unterkunfte(UnterkunftsID));";
		try {
			stmt = c.prepareStatement(sql);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void createTKurs() {
		PreparedStatement stmt;
		String sql = "CREATE TABLE IF NOT EXISTS Kurse(" + "KursID INTEGER NOT NULL," + "Kursname TEXT NOT NULL,"
				+ "Kursart TEXT NOT NULL," + "PRIMARY KEY(KursID));";

		try {
			stmt = c.prepareStatement(sql);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void createTMitg_Kurs() {
		PreparedStatement stmt;
		String sql = "CREATE TABLE IF NOT EXISTS Mitg_Kurs(" + "MitgliedsID INTEGER NOT NULL,"
				+ "KursID INTEGER NOT NULL," + "PRIMARY KEY(MitgliedsID, KursID),"
				+ "FOREIGN KEY(MitgliedsID) REFERENCES Mitglieder(MitgliedsID),"
				+ "FOREIGN KEY(KursID) REFERENCES Kurse(KursID));";

		try {
			stmt = c.prepareStatement(sql);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void createUnterkunft() {
		System.out.println("Geben sie den Namen der Unterkunft ein.");
		String k = sc.next();
		System.out.println("Geben sie das Baujahr der Unterkunft ein.");
		String j = sc.next();
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO Unterkunfte(Unterkunftsname, Baujahr)  VALUES(?,?)";
			stmt = c.prepareStatement(sql);
			stmt.setString(1, k);
			stmt.setString(2, j);
			stmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("FEHLER");
			e.printStackTrace();
		}

		System.out.println("");
	}

	public void createMitglied() {
		System.out.println("Geben sie den Vornamen des Mitglieds ein.");
		String k = sc.next();
		System.out.println("Geben sie den Nachnamen des Mitglieds ein.");
		String j = sc.next();
		System.out.println("Geben sie die ID der Unterkunft ein.");
		int l = sc.nextInt();
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO Mitglieder(Vorname, Nachname, UnterkunftsID)  VALUES(?,?,?)";
			stmt = c.prepareStatement(sql);
			stmt.setString(1, k);
			stmt.setString(2, j);
			stmt.setInt(3, l);
			stmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("FEHLER");
			e.printStackTrace();
		}

		System.out.println("");
	}

	public void createKurs() {
		System.out.println("Geben sie den Namen des Kurses ein.");
		String k = sc.next();
		System.out.println("Geben sie die Art des Kurses ein.");
		String j = sc.next();
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO Kurse(Kursname, Kursart)  VALUES(?,?)";
			stmt = c.prepareStatement(sql);
			stmt.setString(1, k);
			stmt.setString(2, j);
			stmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("FEHLER");
			e.printStackTrace();
		}

		System.out.println("");
	}

	public void createMitg_Kurs() {
		System.out.println("Geben sie die ID des Mitglieds ein.");
		int k = sc.nextInt();
		System.out.println("Geben sie die ID des Kurses ein.");
		int j = sc.nextInt();
		PreparedStatement stmt = null;
		try {
			Statement stmt1 = c.createStatement();
			stmt1.executeUpdate("INSERT INTO Mitg_Kurs(MitgliedsID, KursID) VALUES(1,3)");
			String sql = "INSERT INTO Mitg_Kurs(MitgliedsID, KursID)  VALUES(?,?)";
			stmt = c.prepareStatement(sql);
			stmt.setInt(1, k);
			stmt.setInt(2, j);
			stmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("FEHLER");
			e.printStackTrace();
		}

		System.out.println("");
	}

	public void createUnterkunfteCSV(ArrayList<String> a1, ArrayList<Integer> a2) {
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO Unterkunfte(Unterkunftsname, Baujahr)  VALUES(?,?)";

			for (int x = 0; a1.size() > x; x++) {
				stmt = c.prepareStatement(sql);
				stmt.setString(1, a1.get(x));
				stmt.setInt(2, a2.get(x));
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			System.out.println("FEHLER");
			e.printStackTrace();
		}

		System.out.println("");
	}

	public void createMitgliederCSV(ArrayList<String> a1, ArrayList<String> a2, ArrayList<Integer> a3) {
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO Mitglieder(Vorname, Nachname, UnterkunftsID)  VALUES(?,?,?)";

			for (int x = 0; a1.size() > x; x++) {
				stmt = c.prepareStatement(sql);
				stmt.setString(1, a1.get(x));
				stmt.setString(2, a2.get(x));
				stmt.setInt(3, a3.get(x));
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			System.out.println("FEHLER");
			e.printStackTrace();
		}

		System.out.println("");
	}

	public void createKurseCSV(ArrayList<String> a1, ArrayList<String> a2) {
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO Kurse(Kursname, Kursart)  VALUES(?,?)";

			for (int x = 0; a1.size() > x; x++) {
				stmt = c.prepareStatement(sql);
				stmt.setString(1, a1.get(x));
				stmt.setString(2, a2.get(x));
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			System.out.println("FEHLER");
			e.printStackTrace();
		}

		System.out.println("");
	}

	public void createMitgKursCSV(ArrayList<Integer> a1, ArrayList<Integer> a2) {
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO Mitg_Kurs(MitgliedsID, KursID) VALUES(?,?)";

			for (int x = 0; a1.size() > x; x++) {
				stmt = c.prepareStatement(sql);
				stmt.setInt(1, a1.get(x));
				stmt.setInt(2, a2.get(x));
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			System.out.println("FEHLER");
			e.printStackTrace();
		}

		System.out.println("");
	}
}