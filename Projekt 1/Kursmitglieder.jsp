<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h3>
		Hier werden alle Mitglieder des eingegebenen Kurses angezeigt.
	</h3>
	<%
		String kursname;
		kursname = request.getParameter("kursname1");
	%>
	<%
		out.println("<h3>" + kursname + ":</h3>");
	%>
	<table>
		<thead>
			<tr>
				<th>MitgliedsID</th>
				<th>Vorname</th>
				<th>Nachname</th>
			</tr>
		</thead>
		<tbody>
			<%
				try {
					Class.forName("org.sqlite.JDBC");
					Connection c = DriverManager
							.getConnection("jdbc:sqlite:C:\\sqlite\\sqlite-tools-win32-x86-3200100\\alpenverein.db");
					Statement stmt = c.createStatement();
					ResultSet rs = stmt.executeQuery(
							"SELECT a.MitgliedsID, a.Vorname, a.Nachname FROM Mitglieder a INNER JOIN Mitg_Kurs b ON "
									+ "a.MitgliedsID=b.MitgliedsID INNER JOIN Kurse c ON b.KursID=c.KursID WHERE Kursname='"
									+ kursname + "'");

					while (rs.next()) {
						out.println("<tr>");
						out.println("<td>" + rs.getString("MitgliedsID") + "</td>");
						out.println("<td>" + rs.getString("Vorname") + "</td>");
						out.println("<td>" + rs.getString("Nachname") + "</td>");
						out.println("</tr>");
					}
					rs.close();
					c.close();
				} catch (SQLException e) {
				}
			%>
		</tbody>
	</table>
	<form action="Kurse.jsp" method="get">
		<button type="submit">Zurück zur Kursübersicht</button>
	</form>
</body>
</html>