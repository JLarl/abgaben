<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h3>
		Hier kann man eine Unterkunft suchen.
	</h3>
	<form action="Unterkunftssuche.jsp" method="get">
		<input type="text" name="unterkunftsname" placeholder="Unterkunftsname">
		<button type="submit">suchen</button>
	</form>
	<h3>
		Hier kann man alle Mitglieder in einer Unterkunft anzeigen lassen.
	</h3>
	<form action="Unterkunftsmitglieder.jsp" method="get">
		<input type="text" name="unterkunftsname1" placeholder="Unterkunftsname">
		<button type="submit">suchen</button>
	</form>
	<table>
		<thead>
			<tr>
				<th>UnterkunftsID</th>
				<th>Unterkunftsname</th>
				<th>Baujahr</th>
			</tr> 
		</thead>
		<tbody>
		<%
			try{
			Class.forName("org.sqlite.JDBC");
			Connection c = DriverManager.getConnection("jdbc:sqlite:C:\\sqlite\\sqlite-tools-win32-x86-3200100\\alpenverein.db");
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM Unterkunfte");
			
			while(rs.next()) {
				out.println("<tr>");
				out.println("<td>" + rs.getString("UnterkunftsID") + "</td>");
				out.println("<td>" + rs.getString("Unterkunftsname") + "</td>");
				out.println("<td>" + rs.getString("Baujahr") + "</td>");
				out.println("</tr>");
			}
			rs.close();
			c.close();
			}
			catch(SQLException e){}
		%>
		</tbody>
	</table>
		<form action="Overview.jsp" method="get">
		<button type="submit">Zur�ck zur �bersicht</button>
	</form>
</body>
</html>