import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	private static Scanner inputStream;
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		DBManager db = new DBManager();
		String fileName;

		db.createTUnterkunft();
		db.createTMitglied();
		db.createTKurs();
		db.createTMitg_Kurs();

		boolean schleife = true;
		String st1;
		File file;

		while (schleife == true) {

			System.out.println("Auswahl:");
			System.out.println("1: Mitglieder");
			System.out.println("2: Kurse");
			System.out.println("3: Unterkünfte");
			System.out.println("4: Mitglied-Kurs-Verbindungen");
			System.out.println("5: Datenbank schließen");
			String st2 = sc.next();

			switch (st2) {
			case "1":
				System.out.println("CSV-Datennamen eingeben");
				st1 = sc.next();

				fileName = st1;
				file = new File(fileName);

				ArrayList<String> vornamen = new ArrayList<String>();
				ArrayList<String> nachnamen = new ArrayList<String>();
				ArrayList<String> unterkunftsID1 = new ArrayList<String>();
				ArrayList<Integer> unterkunftsID2 = new ArrayList<Integer>();
				try {
					inputStream = new Scanner(file);
					inputStream.next();
					while (inputStream.hasNext()) {
						String data = inputStream.next();
						System.out.println(data);
						String[] werte = data.split(",");
						vornamen.add(werte[1]);
						nachnamen.add(werte[2]);
						unterkunftsID1.add(werte[3]);
					}
					for (int x = 0; unterkunftsID1.size() > x; x++) {
						unterkunftsID2.add(Integer.parseInt(unterkunftsID1.get(x)));
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					System.out.println("File was not found!");
					db.close();
				}
				db.createMitgliederCSV(vornamen, nachnamen, unterkunftsID2);
				break;

			case "2":
				System.out.println("CSV-Datennamen eingeben");
				st1 = sc.next();

				fileName = st1;
				file = new File(fileName);

				ArrayList<String> kursnamen = new ArrayList<String>();
				ArrayList<String> kursarten = new ArrayList<String>();

				try {
					inputStream = new Scanner(file);
					inputStream.next();
					while (inputStream.hasNext()) {
						String data = inputStream.next();
						System.out.println(data);
						String[] werte = data.split(",");
						kursnamen.add(werte[1]);
						kursarten.add(werte[2]);
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					System.out.println("File was not found!");
					db.close();
				}
				db.createKurseCSV(kursnamen, kursarten);
				break;

			case "3":
				System.out.println("CSV-Datennamen eingeben");
				st1 = sc.next();

				fileName = st1;
				file = new File(fileName);

				ArrayList<String> unterkunftsnamen = new ArrayList<String>();
				ArrayList<String> baujahr1 = new ArrayList<String>();
				ArrayList<Integer> baujahr2 = new ArrayList<Integer>();

				try {
					inputStream = new Scanner(file);
					inputStream.next();
					while (inputStream.hasNext()) {
						String data = inputStream.next();
						System.out.println(data);
						String[] werte = data.split(",");
						unterkunftsnamen.add(werte[1]);
						baujahr1.add(werte[2]);
					}

					for (int x = 0; baujahr1.size() > x; x++) {
						baujahr2.add(Integer.parseInt(baujahr1.get(x)));
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					System.out.println("File was not found!");
					db.close();
				}
				db.createUnterkunfteCSV(unterkunftsnamen, baujahr2);
				break;

			case "4":
				System.out.println("CSV-Datennamen eingeben");
				st1 = sc.next();

				fileName = st1;
				file = new File(fileName);

				ArrayList<String> mitgliedsID1 = new ArrayList<String>();
				ArrayList<Integer> mitgliedsID2 = new ArrayList<Integer>();
				ArrayList<String> kursID1 = new ArrayList<String>();
				ArrayList<Integer> kursID2 = new ArrayList<Integer>();
				try {
					inputStream = new Scanner(file);
					inputStream.next();
					while (inputStream.hasNext()) {
						String data = inputStream.next();
						System.out.println(data);
						String[] werte = data.split(",");
						mitgliedsID1.add(werte[0]);
						kursID1.add(werte[1]);
					}
					for (int x = 0; mitgliedsID1.size() > x; x++) {
						mitgliedsID2.add(Integer.parseInt(mitgliedsID1.get(x)));
					}
					for (int x = 0; kursID1.size() > x; x++) {
						kursID2.add(Integer.parseInt(kursID1.get(x)));
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					System.out.println("File was not found!");
					db.close();
				}
				db.createMitgKursCSV(mitgliedsID2, kursID2);
				break;

			case "5":
				schleife = false;
				break;
			}
		}
		db.close();
	}
}